using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEditor;
using UnityEngine;

namespace HellionCat.Optimizer
{
    /// <summary>
    /// This class implements a custom editor used to optimize debug logs usage in a project
    /// </summary>
    public class OptimizerEditor : EditorWindow
    {
        /// <summary>
        /// A list of all source files in the project.
        /// </summary>
        [SerializeField] private List<string> m_sourceFiles = new List<string>();
        /// <summary>
        /// The data about debug lines for each files
        /// It is not serialized by choice, since we are storing the line number which could change when modifying the code
        /// Thus, we wish to reload the list every time the domain is reloading
        /// </summary>
        private readonly List<FileDebugData> m_filesDebugData = new List<FileDebugData>();
        /// <summary>
        /// The current scroll position for the scroll view
        /// </summary>
        [SerializeField] private Vector2 m_scrollPos;
        /// <summary>
        /// The selected tab
        /// </summary>
        [SerializeField] int m_currentTab;
        /// <summary>
        /// A bool showing whether the loading bar is active or not
        /// </summary>
        private static bool s_loading;
        /// <summary>
        /// The text to display on the progress bar when active
        /// </summary>
        private static string s_loadingText = "Loading";
        /// <summary>
        /// The percentage to display on the progress bar when active
        /// </summary>
        private static float s_loadingPct = .5f;
        /// <summary>
        /// Used to tell we wish to display the loading bar for a commenting operation
        /// </summary>
        private static bool s_commentOperation;
    
        /// <summary>
        /// Show the window when clicking the button
        /// </summary>
        [MenuItem ("Optimizer/Debug logs editor")]
        public static void  ShowWindow () {
            GetWindow(typeof(OptimizerEditor));
        }

        /// <summary>
        /// Handle the editor gui logic
        /// </summary>
        private void OnGUI ()
        { 
            //Change the bold label alignment to center it
            EditorStyles.boldLabel.alignment = TextAnchor.MiddleCenter;
        
            //Write the title of the editor
            EditorGUILayout.LabelField("Debug logs manager", EditorStyles.boldLabel);

            //Get the current with of the editor, which will be used to handle the width of each items in it.
            var l_currentWidth = EditorGUIUtility.currentViewWidth;

            //Get a dynamic label for the search button based on the current state of the data
            var l_buttonLabel = m_filesDebugData.Count > 0 ? "Re-search for debug logs in code" : "Search for debug logs in code";

            //Create a button used to find all debug logs line in the source files
            if (GUILayout.Button(l_buttonLabel))
            {
                s_loading = true;
                //Start searching for the debug lines in another thread, to avoid hanging the ui thread
                new Thread(() => 
                {
                    //Start the loading process
                
                    UpdateLoadingStatus(.1f,"Initializing...");
                    
                    //Indicate that this thread is a background thread
                    Thread.CurrentThread.IsBackground = true; 
            
                    //Clear the lists before recharging them
                    m_sourceFiles.Clear();
                    m_filesDebugData.Clear();
        
                    //Get the assets folder of the project
                    var l_projectDir = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "Assets";

                    UpdateLoadingStatus(.2f,"Analyzing directories...");
                    //Find all source files in the project
                    HandleDirectory(l_projectDir);
                        
                    UpdateLoadingStatus(.5f,"Analyzing files...");
                    //Find all debug lines in the source files
                    foreach (var l_sourceFile in m_sourceFiles)
                    {
                        FindDebugInFiles(l_sourceFile);
                    }
                
                    UpdateLoadingStatus(1f,"Finalizing...");
                    s_loading = false;
                }).Start();
            }

            //Display a loading bar to display when loading data or commenting
            if (s_loading || s_commentOperation)
            {
                var l_rect = EditorGUILayout.BeginVertical();
                EditorGUI.ProgressBar(l_rect, s_loadingPct,s_loadingText);     
                GUILayout.Space(16);
                EditorGUILayout.EndVertical();
            }

            //The following part of the editor should not be shown without data
            if (m_filesDebugData.Count <= 0) return;
        
            //It should also not be shown while loading
            if (s_loading) return;

            //This allow to display tabs on top of the gui to allow sorting between the != types of logs
            m_currentTab = GUILayout.Toolbar (m_currentTab, new[] {"All", "Logs", "Warnings", "Errors"});

            //Swo two buttons allowing to comment/uncomment all logs in the current tab
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("");
        
            if (GUILayout.Button("Comment all"))
            {

                if (EditorUtility.DisplayDialog("Comment all debug logs?",
                    "Are you sure you wish to comment all debug logs?\nErrors may appear in the code files.\n" +
                    "Please refer to the documentation fo more information.", "Yes", "No"))
                {
//Start the commenting operation in a thread to avoid hanging the ui
                    s_commentOperation = true;
                    new Thread(() => 
                    {
                        //Start the commenting process
                        UpdateLoadingStatus(0f,"Initializing...");
                    
                        //Indicate that this thread is a background thread
                        Thread.CurrentThread.IsBackground = true; 
                
                        //Create an array variable to store the sorted logs in the current file
                        DebugLineData[] l_dataArray = null;

                        //Get the amount of files to allow having a better loading bar
                        var l_fileAmount = m_filesDebugData.Count;
                        var l_currentCount = 0f;
                
                        //For each file, we are getting the logs with the type corresponding to the current tab 
                        foreach (var l_fileDebugData in m_filesDebugData)
                        {
                            l_currentCount++;
                            UpdateLoadingStatus(l_currentCount/l_fileAmount,"Processing "+l_fileDebugData.m_file+"...");
                            switch (m_currentTab)
                            {
                                case 0:
                                    l_dataArray = l_fileDebugData.m_debugLineData;
                                    break;
                                case 1:
                                    l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Log).ToArray();
                                    break;
                                case 2:
                                    l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Warning).ToArray();
                                    break;
                                case 3:
                                    l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Error).ToArray();
                                    break;
                            }

                            CommentGroupOfLines(l_dataArray, l_fileDebugData.m_file);
                        }
                        //Finish the commenting process
                        UpdateLoadingStatus(1f,"Finalizing...");
                        s_commentOperation = false;
                    }).Start();
                }
            }
        
            //Repeat the same has above, but with uncommenting instead of commenting
            if (GUILayout.Button("Uncomment all"))
            {
                if (EditorUtility.DisplayDialog("Uncomment all debug logs?",
                    "Are you sure you wish to uncomment all debug logs?\nErrors may appear in the code files.\n" +
                    "Please refer to the documentation fo more information", "Yes", "No"))
                {
                    s_commentOperation = true;
                    new Thread(() => 
                    {
                        //Start the loading process
                        UpdateLoadingStatus(0f,"Initializing...");
                    
                        //Indicate that this thread is a background thread
                        Thread.CurrentThread.IsBackground = true; 
                        DebugLineData[] l_dataArray = null;

                        var l_fileAmount = m_filesDebugData.Count;
                        var l_currentCount = 0f;
                
                        foreach (var l_fileDebugData in m_filesDebugData)
                        {
                            l_currentCount++;
                            UpdateLoadingStatus(l_currentCount/l_fileAmount,"Processing "+l_fileDebugData.m_file+"...");
                            switch (m_currentTab)
                            {
                                case 0:
                                    l_dataArray = l_fileDebugData.m_debugLineData;
                                    break;
                                case 1:
                                    l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Log).ToArray();
                                    break;
                                case 2:
                                    l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Warning).ToArray();
                                    break;
                                case 3:
                                    l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Error).ToArray();
                                    break;
                            }

                            UncommentGroupOfLines(l_dataArray, l_fileDebugData.m_file);
                        }
                        UpdateLoadingStatus(1f,"Finalizing...");
                        s_commentOperation = false;
                    }).Start();
                }
            }
        
            EditorGUILayout.LabelField("");
            EditorGUILayout.EndHorizontal();

            //While commenting we are not displaying the lists of logs
            if (s_commentOperation)
                return;
        
            //Start a scroll view which will hold the files and their debug lines
            m_scrollPos =
                EditorGUILayout.BeginScrollView(m_scrollPos, false,false);

            //We are going to show each file name separately, with their debug logs under it
            foreach (var l_fileDebugData in m_filesDebugData)
            {
                //We are skipping the files who does not have any debug lines
                if(l_fileDebugData.m_debugLineData == null || l_fileDebugData.m_debugLineData.Length == 0)
                    continue;

                //Sort the data to only get the logs corresponding to the current tab
                DebugLineData[] l_dataArray = null;
                switch (m_currentTab)
                {
                    case 0:
                        l_dataArray = l_fileDebugData.m_debugLineData;
                        break;
                    case 1:
                        l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Log).ToArray();
                        break;
                    case 2:
                        l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Warning).ToArray();
                        break;
                    case 3:
                        l_dataArray = l_fileDebugData.m_debugLineData.Where(p_d => p_d.m_optimizerLogType == OptimizerLogType.Error).ToArray();
                        break;
                }

                if (l_dataArray == null || l_dataArray.Length == 0)
                    continue;
            
                //Get the file name from the complete path
                var l_fileNameData = l_fileDebugData.m_file.Split('\\');
                var l_fileName = l_fileNameData[l_fileNameData.Length - 1];

                //If we are in the general tab, show a colored symbols to let the user know what kind of logs are in a file
                if (m_currentTab == 0)
                {
                    if (l_fileDebugData.m_debugLineData.Count(p_d => p_d.m_optimizerLogType == OptimizerLogType.Error)  > 0)
                    {
                        l_fileName = "<color=#CC0000FF>[E]</color> " + l_fileName;
                    }
            
                    if (l_fileDebugData.m_debugLineData.Count(p_d => p_d.m_optimizerLogType == OptimizerLogType.Warning)  > 0)
                    {
                        l_fileName = "<color=#7F7F00FF>[W]</color> " + l_fileName;
                    }
            
                    if (l_fileDebugData.m_debugLineData.Count(p_d => p_d.m_optimizerLogType == OptimizerLogType.Log) > 0)
                    {
                        l_fileName = "[L] " + l_fileName;
                    }
                }
            
                //Create a foldout with the file name as label
                var l_foldoutStyle = new GUIStyle(EditorStyles.foldout) {richText = true};
                l_fileDebugData.m_foldout = EditorGUILayout.Foldout(l_fileDebugData.m_foldout, l_fileName, true, l_foldoutStyle);
                if (!l_fileDebugData.m_foldout) continue;
            
                //Write the header used for the debug lines "table"
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Line", GUILayout.Width(l_currentWidth/1.5f));
                EditorGUILayout.LabelField("Is commented", GUILayout.Width(l_currentWidth/3.9f));
                EditorGUILayout.EndHorizontal();
            
                //Write each debug logs line under each others
                foreach (var l_data in l_dataArray)
                {
                    //Wrap each debug log in an horizontal layout to display the line and it's state on the same line
                    EditorGUILayout.BeginHorizontal();

                    var l_color = l_data.m_optimizerLogType == OptimizerLogType.Error ? new Color(0.8f, 0f, 0f) :
                        l_data.m_optimizerLogType == OptimizerLogType.Warning ? new Color(0.5f, 0.5f, 0f) : Color.black;

                    EditorStyles.label.normal.textColor = l_color;

                    //Write the debug line with it's line number(s)
                    EditorGUILayout.LabelField(l_data.m_condensedString, GUILayout.Width(l_currentWidth / 1.5f));

                    //Get the state of the line before handling the comment toggle
                    var l_wasCommented = l_data.m_commented;

                    //Show a toggle that will allow to comment/uncomment the debug line
                    l_data.m_commented =
                        GUILayout.Toggle(l_data.m_commented, "", GUILayout.Width(l_currentWidth / 3.9f));

                    //Handle the state of the debug line if it has changed, in threads since reading/writing a file is
                    //an expensive operation
                    if (l_wasCommented && !l_data.m_commented)
                    {
                    
                        new Thread(() => 
                        {
                            UpdateLoadingStatus(0f,"Initializing...");
                            UncommentDebugLine(l_data, l_fileDebugData.m_file);
                            UpdateLoadingStatus(1f,"Finalizing...");
                            s_commentOperation = false;
                        }).Start();
                    }
                    else if (!l_wasCommented && l_data.m_commented)
                    {
                        new Thread(() => 
                        {
                            UpdateLoadingStatus(0f,"Initializing...");
                            CommentDebugLine(l_data, l_fileDebugData.m_file);
                            UpdateLoadingStatus(1f,"Finalizing...");
                            s_commentOperation = false;
                        }).Start();
                    }


                    EditorStyles.label.normal.textColor = Color.black;
                    EditorGUILayout.EndHorizontal();
                    //Show a separator line to better see each debug line
                    EditorGUILayout.LabelField("", GUI.skin.horizontalSlider, GUILayout.Width(l_currentWidth / 1.05f));
                }
            }
            //End the scroll view containing the files 
            EditorGUILayout.EndScrollView();
        }

        #region CommandLineEditor

        /// <summary>
        /// Comment an entire array of lines, used when using comment all buttons.
        /// </summary>
        /// <param name="p_data">An array containing the data of the lines you wish to comments</param>
        /// <param name="p_file">The file that holds the line</param>
        private static void CommentGroupOfLines(DebugLineData[] p_data,string p_file)
        {
            foreach (var l_debugLineData in p_data)
            {
                if(l_debugLineData.m_commented) continue;
            
                l_debugLineData.m_commented = true;
                CommentDebugLine(l_debugLineData,p_file);
            }
        }
    
        /// <summary>
        /// Uncomment an entire array of lines, used when using uncomment all buttons.
        /// </summary>
        /// <param name="p_data">An array containing the data of the lines you wish to uncomments</param>
        /// <param name="p_file">The file that holds the line</param>
        private static void UncommentGroupOfLines(DebugLineData[] p_data,string p_file)
        {
            foreach (var l_debugLineData in p_data)
            {
                if(!l_debugLineData.m_commented) continue;
            
                l_debugLineData.m_commented = false;
                UncommentDebugLine(l_debugLineData,p_file);
            }
        }
    
        /// <summary>
        /// Remove the commenting char of a debug line.
        /// </summary>
        /// <param name="p_data">The data of the line you wish to uncomment</param>
        /// <param name="p_file">The file in which the line can be found</param>
        private static void UncommentDebugLine(DebugLineData p_data, string p_file)
        {
            //Uncomment the debug line. Use a different method depending if the debug log is on a single line or not.
            if (p_data.m_startLine == p_data.m_endLine)
            {
                if (!UncommentSingleLine(p_data.m_startLine, p_file))
                {
                    //If we are in a special case where the debug log is inside a commented block,
                    //we are not uncommenting it to avoid issues
                    p_data.m_commented = true;
                }
            }
            else
            {
                UncommentMultipleLine(p_data.m_startLine, p_data.m_endLine, p_file);
            }
        }

        /// <summary>
        /// Uncomment a single line comment
        /// </summary>
        /// <param name="p_line">The number of the line you wish to uncomment</param>
        /// <param name="p_file">The file in which the line can be found</param>
        private static bool UncommentSingleLine(int p_line, string p_file)
        {
            //Load all the lines of the file in memory
            var l_arrLine = File.ReadAllLines(p_file);
        
            //Check if the previous or next line are commented, in which case we are not uncommenting the line
            //To avoid case when a debug log is in a commented function, which would cause an error
            //Or depend on a commented var before
            var l_lastLine = l_arrLine[p_line - 2];

            if (l_lastLine.Contains("//")
                || l_lastLine.Contains("*")
                || l_lastLine.Contains("/*"))
            {
                return false;
            }
        
            //Find and remove the commenting char in the debug log.
            //Handle each possible case of commenting

            //This is a special case we are creating that happen when commenting a debug log which as code before it
            //We are adding an additional ; to try to avoid error in code as much as possible
        
            if (l_arrLine[p_line - 1].Contains("//"))
            {
                l_arrLine[p_line - 1] = l_arrLine[p_line - 1].Replace("//","").Trim();
            }

            if (l_arrLine[p_line - 1].Contains("/*"))
            {
                l_arrLine[p_line - 1] = l_arrLine[p_line - 1].Replace("/*","").Trim();
            }

            if (l_arrLine[p_line - 1].Contains("*/"))
            {
                l_arrLine[p_line - 1] = l_arrLine[p_line - 1].Replace("*/","").Trim();
            }
        
            //Rewrite all the new lines in the file 
            File.WriteAllLines(p_file, l_arrLine);
        
            return true;
        }

        /// <summary>
        /// Uncomment a multiple line comment
        /// </summary>
        /// <param name="p_startLine">The number of the first line of the debug log you wish to uncomment</param>
        /// <param name="p_endLine">The number of the last line of the debug log you wish to uncomment</param>
        /// <param name="p_file">The file in which the lines can be found</param>
        private static void UncommentMultipleLine(int p_startLine, int p_endLine, string p_file)
        {
            //Load all the lines of the file in memory
            var l_arrLine = File.ReadAllLines(p_file);
        
            //For each lines between the first and last line of the debug log, remove the possibles commenting char
            for (var l_i = p_startLine - 1; l_i < p_endLine; l_i++)
            {
            
                if (l_arrLine[l_i].Contains("/*"))
                {
                    l_arrLine[l_i] = l_arrLine[l_i].Replace("/*","").Trim();
                }
            
                if (l_arrLine[l_i].Contains("*/"))
                {
                    l_arrLine[l_i] = l_arrLine[l_i].Replace("*/","").Trim();
                }

                if (l_arrLine[l_i].Contains("*"))
                {
                    l_arrLine[l_i] = l_arrLine[l_i].Replace("*","").Trim();
                }
            }
        
            //Rewrite all the new lines in the file 
            File.WriteAllLines(p_file, l_arrLine);
        }
    
        /// <summary>
        /// Commenting a debug line.
        /// </summary>
        /// <param name="p_data">The data of the line you wish to comment</param>
        /// <param name="p_file">The file in which the line can be found</param>
        private static void CommentDebugLine(DebugLineData p_data, string p_file)
        {
            //Comment the debug line. Use a different method depending if the debug log is on a single line or not.
            if (p_data.m_startLine == p_data.m_endLine)
            {
                CommentSingleLine(p_data.m_startLine, p_file);
            }
            else
            {
                CommentMultipleLine(p_data.m_startLine, p_data.m_endLine, p_file);
            }
        }

        /// <summary>
        /// Comment a single line debug log
        /// </summary>
        /// <param name="p_line">The number of the line you wish to comment</param>
        /// <param name="p_file">The file in which the line can be found</param>
        private static void CommentSingleLine(int p_line, string p_file)
        {
            //Load all the lines of the file in memory
            var l_arrLine = File.ReadAllLines(p_file);

            //This bool check if there is a need to surround the comments with brackets, to handle special case
            //such as if() debug.log();
            var l_needBrackets = 
                (p_line - 2 > 0 && p_line - 2 < l_arrLine.Length && l_arrLine[p_line - 2].Contains("if") 
                && !l_arrLine[p_line - 2].Contains("{") && l_arrLine[p_line - 1].Trim()[0] != '{' )
                || (p_line - 2 > 0 && p_line - 2 < l_arrLine.Length && l_arrLine[p_line - 2].Contains("else") 
                && !l_arrLine[p_line - 2].Contains("{") && l_arrLine[p_line - 1].Trim()[0] != '{' )
                || (l_arrLine[p_line - 1].Contains("if") && !l_arrLine[p_line - 1].Contains('{') )
                || (l_arrLine[p_line - 1].Contains("else") && l_arrLine[p_line - 1].Contains('{') )
                || (l_arrLine[p_line - 1].Contains("=>") && !l_arrLine[p_line - 1].Contains("{"));

            //Find the character where the debug log start
            var l_start = 0;
            for (var l_i = 0; l_i < l_arrLine[p_line - 1].Length; l_i++)
            {
                if (l_arrLine[p_line - 1].Length >= l_i+21 && l_arrLine[p_line - 1].Substring(l_i,21) == "UnityEngine.Debug.Log")
                {
                    l_start = l_i;
                    break;
                }
            
                if (l_arrLine[p_line - 1].Substring(l_i,9) == "Debug.Log")
                {
                    l_start = l_i;
                    break;
                }
            }

            //Check if the debug log is taking the entire line, or if there is something else before
            var l_fullLine = string.IsNullOrWhiteSpace(l_arrLine[p_line - 1].Substring(0, l_start));

            //Find the character where the log end
            var l_end = 0;
            for (var l_i = 0; l_i < l_arrLine[p_line - 1].Length; l_i++)
            {
                //Handle case when the log is inside a lambda
                if (!l_fullLine && l_arrLine[p_line - 1].Length >= l_i+3 && l_arrLine[p_line - 1].Substring(l_i,3) == "));")
                {
                    l_end = l_i+1;
                    break;
                }
            
                if (l_arrLine[p_line - 1].Substring(l_i,2) == ");")
                {
                    l_end = l_i+2;
                    break;
                }
            }

            //Handle the need of brackets in the debug logs
            if (l_needBrackets)
            {        
                //Add the commenting char at the start of the line
                l_arrLine[p_line - 1] = l_arrLine[p_line - 1].Substring(0,l_start)+"{/*"+l_arrLine[p_line - 1].Substring(l_start,l_end-l_start)+"*/}"+l_arrLine[p_line - 1].Substring(l_end);
            }
            else
            {
                //Add the commenting char at the start of the line
                l_arrLine[p_line - 1] = l_arrLine[p_line - 1].Substring(0,l_start)+"/*"+l_arrLine[p_line - 1].Substring(l_start,l_end-l_start)+"*/"+l_arrLine[p_line - 1].Substring(l_end);
            }
        
        
            //Rewrite all the new lines in the file 
            File.WriteAllLines(p_file, l_arrLine);
        }

        /// <summary>
        /// Uncomment a multiple line debug log
        /// </summary>
        /// <param name="p_startLine">The number of the first line of the debug log you wish to comment</param>
        /// <param name="p_endLine">The number of the last line of the debug log you wish to comment</param>
        /// <param name="p_file">The file in which the lines can be found</param>
        private static void CommentMultipleLine(int p_startLine, int p_endLine, string p_file)
        {
            //Load all the lines of the file in memory
            var l_arrLine = File.ReadAllLines(p_file);
        
            //This bool check if there is a need to surround the comments with brackets, to handle special case
            //such as if() debug.log();
            var l_needBrackets = 
                p_startLine - 2 > 0 && p_startLine - 2 < l_arrLine.Length && l_arrLine[p_startLine - 2].Contains("if") 
                && !l_arrLine[p_startLine - 2].Contains("{") && l_arrLine[p_startLine - 1].Trim()[0] != '{' 
                || p_startLine - 2 > 0 && p_startLine - 2 < l_arrLine.Length && l_arrLine[p_startLine - 2].Contains("else") 
                && !l_arrLine[p_startLine - 2].Contains("{") && l_arrLine[p_startLine - 1].Trim()[0] != '{' 
                || l_arrLine[p_startLine - 1].Contains("if") && l_arrLine[p_startLine - 1].Contains('{') 
                || l_arrLine[p_startLine - 1].Contains("else") && l_arrLine[p_startLine - 1].Contains('{')
                || l_arrLine[p_startLine - 1].Contains("=>") && !l_arrLine[p_startLine - 1].Contains("{");

            //Find the character where the debug log start
            var l_start = 0;
            for (var l_i = 0; l_i < l_arrLine[p_startLine - 1].Length; l_i++)
            {
                if (l_arrLine[p_startLine - 1].Length >= l_i+21 && l_arrLine[p_startLine - 1].Substring(l_i,21) == "UnityEngine.Debug.Log")
                {
                    l_start = l_i;
                    break;
                }
            
                if (l_arrLine[p_startLine - 1].Substring(l_i,9) == "Debug.Log")
                {
                    l_start = l_i;
                    break;
                }
            }

            //Handle the need of brackets in the debug logs
            if (l_needBrackets)
            {
                //Add the commenting start char at the start of the starting line
                l_arrLine[p_startLine - 1] = l_arrLine[p_startLine - 1].Substring(0,l_start)+"{/*"+l_arrLine[p_startLine - 1].Substring(l_start);
            }
            else
            {
                //Add the commenting start char at the start of the starting line
                l_arrLine[p_startLine - 1] = l_arrLine[p_startLine - 1].Substring(0,l_start)+"/*"+l_arrLine[p_startLine - 1].Substring(l_start);
            }
        
            //Check if the debug log is taking the entire line, or if there is something else before
            var l_fullLine = string.IsNullOrWhiteSpace(l_arrLine[p_startLine - 1].Substring(0, l_start));
        
            //Find the character where the log end
            var l_end = 0;
            for (var l_i = 0; l_i < l_arrLine[p_endLine - 1].Length; l_i++)
            {
                //Handle case when the log is inside a lambda
                if (!l_fullLine && l_arrLine[p_endLine - 1].Length >= l_i+3 &&  l_arrLine[p_endLine - 1].Substring(l_i,3) == "));")
                {
                    l_end = l_i+1;
                    break;
                }
            
                if (l_arrLine[p_endLine - 1].Substring(l_i,2) == ");")
                {
                    l_end = l_i+2;
                    break;
                }
            }

            //Handle the need of brackets in the debug logs
            if (l_needBrackets)
            {
                //Add the commenting end char at the end of the ending line
                l_arrLine[p_endLine - 1] = l_arrLine[p_endLine - 1].Substring(0,l_end)+"*/}"+l_arrLine[p_endLine - 1].Substring(l_end);
            }
            else
            {
                //Add the commenting end char at the end of the ending line
                l_arrLine[p_endLine - 1] = l_arrLine[p_endLine - 1].Substring(0,l_end)+"*/"+l_arrLine[p_endLine - 1].Substring(l_end);
            }
        
            //Rewrite all the new lines in the file 
            File.WriteAllLines(p_file, l_arrLine);
        }

        /// <summary>
        /// Find the debug logs in a specific file
        /// </summary>
        /// <param name="p_file"></param>
        private void FindDebugInFiles(string p_file)
        {
            //Read all the lines in the file
            var l_lines = File.ReadLines(p_file);
                        
            UpdateLoadingStatus(.7f,"Analyzing "+p_file+"...");
        
            //Create a starting file debug data object
            var l_fileDebugData = new FileDebugData();
            //Create the list that will contains all the debug lines for the current file
            var l_debugLineData = new List<DebugLineData>();

            //Create a counter variable used to keep track of the current line in the file
            var l_counter = 1;

            //Create a starting debug line data object
            var l_data = new DebugLineData();
        
            //Keep track whether the current line is part of a multiple line debug log
            var l_isPartOfDebug = false;
        
            //Loop on each line in the files
            foreach (var l_line in l_lines)
            {
                //Get the current line in a non-readonly variable
                var l_writtenLine = l_line;

                var l_isLog = false;

                //Detect if the line is starting a debug log, and which type of log
                if (l_writtenLine.Contains("Debug.Log("))
                {
                    l_isLog = true;
                    l_data.m_optimizerLogType = OptimizerLogType.Log;
                }
                else if (l_writtenLine.Contains("Debug.LogWarning("))
                {
                    l_isLog = true;
                    l_data.m_optimizerLogType = OptimizerLogType.Warning;
                }
                else if (l_writtenLine.Contains("Debug.LogError("))
                {
                    l_isLog = true;
                    l_data.m_optimizerLogType = OptimizerLogType.Error;
                }
            
                //If the current line not starting a new debug log, or not part of a debug log we can skip it
                if (!l_isLog && !l_isPartOfDebug)
                {
                    //Increase the line counter to keep track of the current line
                    l_counter++;
                    continue;
                }
            
                //Handle the case in which the current line is not the closing line
                if (!l_writtenLine.Contains(");"))
                {
                    //If we are not a closing line, but also not part of an already started debug log,
                    //it means we are the first line of the debug log
                    if (!l_isPartOfDebug)
                    {
                        //Save the starting line of the debug log in the data object
                        l_data.m_startLine = l_counter;
                        //Set the debug bool to true
                        l_isPartOfDebug = true;

                        //Handle the case where the debug log is already commented
                        if (l_writtenLine.Contains("/*"))
                        {
                            //Remove the commenting char from the string displayed in the editor
                            l_writtenLine = l_writtenLine.Replace("/*","").Trim();
                            //Set the commented bool to true
                            l_data.m_commented = true;
                        }
                    }
            
                    //Write the current line in the string that will be displayed in the editor
                    l_data.m_condensedString += l_writtenLine.Trim();
                }
                else
                {
                    //Hand the case of a single line comment that is already commented
                    if (l_writtenLine.Contains("//"))
                    { 
                        //Remove the commenting char from the string displayed in the editor
                        l_writtenLine = l_writtenLine.Replace("//","").Trim();
                        //Set the commented bool to true
                        l_data.m_commented = true;
                    }
                    
                    if (l_writtenLine.Contains("/*"))
                    {
                        //Remove the commenting char from the string displayed in the editor
                        l_writtenLine = l_writtenLine.Replace("/*","").Trim();
                        //Set the commented bool to true
                        l_data.m_commented = true;
                    }
                
                    //Set the starting line in case of a single line debug log
                    if (l_data.m_startLine == 0)
                    {
                        l_data.m_startLine = l_counter;
                    }
                
                    //Set the end line of the debug log in the data object
                    l_data.m_endLine = l_counter;
                    //Reset the boolean
                    l_isPartOfDebug = false;

                    //Remove the commenting char from the string displayed in the editor
                    if (l_writtenLine.Contains("*/"))
                    {
                        l_writtenLine = l_writtenLine.Replace("*/","").Trim();
                    }
            
                    //Write the current line in the string that will be displayed in the editor
                    l_data.m_condensedString += l_writtenLine.Trim();

                    //Finish the string that will be displayed in the editor by adding the line(s) number(s) to it
                    if (l_data.m_startLine == l_data.m_endLine)
                    {
                        l_data.m_condensedString = "["+l_data.m_startLine + "] : " + l_data.m_condensedString;
                    }
                    else
                    {
                        l_data.m_condensedString = "["+l_data.m_startLine+","+l_data.m_endLine+"] : " + l_data.m_condensedString;
                    }
            
                    //Add the current debug line data to the list
                    l_debugLineData.Add(l_data);
                
                    //Create a new debug line data object for the next debug log
                    l_data = new DebugLineData();
                }
            
                //Increase the line counter to keep track of the current line
                l_counter++;
            }

            //Finally, write the information into the file debug data object
            l_fileDebugData.m_file = p_file;
            l_fileDebugData.m_debugLineData = l_debugLineData.ToArray();
            //And add it to the list to be displayed in the editor
            m_filesDebugData.Add(l_fileDebugData);
        }

        /// <summary>
        /// Handle a directory, getting all files in it and handling its inner directories recursively
        /// </summary>
        /// <param name="p_path">The path of the directory you wish to handle</param>
        private void HandleDirectory(string p_path)
        {
            //In case the directory does not exists, we just ignore it
            if (!Directory.Exists(p_path)) return;
        
            UpdateLoadingStatus(.3f,"Analyzing "+p_path+"...");
        
            //Get all the directories in the current directory
            var l_directories = Directory.GetDirectories(p_path);

            //Loop on each directory in the current one to handle them
            foreach (var l_directory in l_directories)
            {
                HandleDirectory(l_directory);
            }

            //Get all the source files in the current directory
            GetSourceFilesInDirectory(p_path);
        }

        /// <summary>
        /// Get all the files in the given directory
        /// </summary>
        /// <param name="p_path">The path of the directory for which you wish to get all files</param>
        private void GetSourceFilesInDirectory(string p_path)
        {
            //In case the directory does not exists, we just ignore it
            if (!Directory.Exists(p_path)) return;
        
            //Loop on each file in the directory
            foreach (var l_file in Directory.GetFiles(p_path))
            {
                //Skip all files that are not a source file
                if (!l_file.EndsWith(".cs")) continue;
            
                //Skip this file to avoid issues
                if(!l_file.Contains(GetType().Name))
                    m_sourceFiles.Add(l_file);
            }
        }   

        #endregion

        #region LoadingBar

        /// <summary>
        /// Update the status of the loading bar
        /// </summary>
        /// <param name="p_loadingPct">The percentage of the loading bar, between 0 and 1</param>
        /// <param name="p_loadingText">The text to display on the loading bar</param>
        private static void UpdateLoadingStatus(float p_loadingPct, string p_loadingText)
        {
            s_loadingPct = p_loadingPct;
            s_loadingText = p_loadingText;
        }

        #endregion
    }

    /// <summary>
    /// This class hold data about all debug logs in a file
    /// </summary>
    [Serializable]
    public class FileDebugData
    {
        /// <summary>
        /// The data about each debug logs in the file
        /// </summary>
        public DebugLineData[] m_debugLineData;
        /// <summary>
        /// The path of the file
        /// </summary>
        public string m_file;
        /// <summary>
        /// Whether the file has been fold out in the editor
        /// </summary>
        public bool m_foldout;
    }

    /// <summary>
    /// This class hold data about a debug logs
    /// </summary>
    [Serializable]
    public class DebugLineData
    {
        /// <summary>
        /// The line at which the debug log is starting
        /// </summary>
        public int m_startLine;
        /// <summary>
        /// The line at which the debug log is ending
        /// </summary>
        public int m_endLine;
        /// <summary>
        /// The condensed string used to display the debug line in the editor
        /// </summary>
        public string m_condensedString;
        /// <summary>
        /// Whether the line is commented in the source file
        /// </summary>
        public bool m_commented;
        /// <summary>
        /// The type of the debug log
        /// </summary>
        public OptimizerLogType m_optimizerLogType;
    }

    /// <summary>
    /// The different types of possible debug logs
    /// </summary>
    public enum OptimizerLogType
    {
        /// <summary>
        /// A Debug.Log()
        /// </summary>
        Log,
        /// <summary>
        /// A Debug.LogWarning()
        /// </summary>
        Warning,
        /// <summary>
        /// A Debug.LogError()
        /// </summary>
        Error
    }
}