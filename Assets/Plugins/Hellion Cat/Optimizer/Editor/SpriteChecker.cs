using System.Globalization;
using System.IO;
using UnityEditor;
using UnityEngine;
using static UnityEditor.EditorWindow;

namespace HellionCat.Optimizer
{
    public class SpriteChecker : EditorWindow
    {
        /// <summary>
        /// Filter that allows us to only query the files in the project
        /// </summary>
        private readonly string[] m_filter = { "Assets" };
        /// <summary>
        /// The name of each tabs in the editor
        /// </summary>
        private const string Column = "Path;Dimension;Recommended;Message";
        /// <summary>
        /// The path where the csv report should be saved
        /// </summary>
        private const string Path = "/Optimizer_TextureReport.csv";
        /// <summary>
        /// The path of the backup folder chosen by the user
        /// </summary>
        private string m_backUpPath;
        /// <summary>
        /// Allow to generate a report (csv file) after checking 
        /// </summary>
        private bool m_AllowToGenerateReport = true;
        /// <summary>
        /// The path of the Assets folder
        /// </summary>
        private string m_assetPath;
        /// <summary>
        /// The selected window
        /// </summary>
        private int m_currentWindow;
        /// <summary>
        /// The number of files that need to be optimized
        /// </summary>
        private int m_fileNumber;
        /// <summary>
        /// An array that contains the data of all the assets
        /// </summary>
        private TextureData[] m_textures;
        /// <summary>
        /// The current scroll position for the scroll view
        /// </summary>
        private Vector2 m_scroll;
        /// <summary>
        /// GUIStyle for the asset unsupported by this plugin
        /// </summary>
        public GUIStyle m_notSupStyle;
        /// <summary>
        /// GUIStyle for the asset supported depending on the state of the file
        /// </summary>
        public GUIStyle[] m_statusStyle = new GUIStyle[3];
        /// <summary>
        /// Define if the option "Set all readable" is on
        /// </summary>
        private bool m_allReadable;

        /// <summary>
        /// Show the window when clicking the button
        /// </summary>
        [MenuItem("Optimizer/Sprite Checker")]
        public static void ShowWindow()
        {
            GetWindow(typeof(SpriteChecker));
        }

        /// <summary>
        /// Initialize the editor window
        /// </summary>
        private void OnEnable()
        {
            //set up the window
            m_allReadable = false;
            m_assetPath = Application.dataPath + Path;

            //set up the GUIStyles
            try
            {
                var l_color = new Color(0.9f, 0, 0.9f);

                m_notSupStyle = new GUIStyle(EditorStyles.foldout)
                {
                    normal = {textColor = l_color},
                    onNormal = {textColor = l_color},
                    focused = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                    onFocused = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                    onHover = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                    hover = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                    onActive = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                    active = {textColor = Color.Lerp(l_color, Color.black, 0.2f)}
                };

                for (var l_i = 0; l_i < 3; l_i++)
                {
                    switch (l_i)
                    {
                        case 0:
                            l_color = new Color(0, 0.65f, 0);
                            break;
                        case 1:
                            l_color = Color.red;
                            break;
                        default:
                            l_color = new Color(0.9f, 0.45f, 0);
                            break;
                    }

                    m_statusStyle[l_i] = new GUIStyle(EditorStyles.foldout)
                    {
                        normal = {textColor = l_color},
                        onNormal = {textColor = l_color},
                        focused = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                        onFocused = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                        onHover = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                        hover = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                        onActive = {textColor = Color.Lerp(l_color, Color.black, 0.2f)},
                        active = {textColor = Color.Lerp(l_color, Color.black, 0.2f)}
                    };
                }
            }
            catch
            {
                // ignored
            }

            //get the path of the backup file
            m_backUpPath = PlayerPrefs.GetString("Opti_backupFolder", "");
            m_AllowToGenerateReport = PlayerPrefs.GetInt("Opti_allowReport", 1)==1;
        }

        /// <summary>
        /// Reset the readonly state of each textures back to the default one selected by the user when the editor is closed
        /// </summary>
        private void OnDisable()
        {
            //reset the options "readable" if the user forgot it
            if (m_allReadable)
            {
                Debug.LogWarning("You forgot to reset the readable options before leaving. Dont worry, we will do it for you.");
                for (var l_i = 0; l_i < m_textures.Length; l_i++)
                {
                    if (m_textures[l_i].Status == SpriteStatus.NotMultiple)
                    {
                        m_textures[l_i].Importer.isReadable = m_textures[l_i].IsReadable;
                        m_textures[l_i].Importer.SaveAndReimport();
                    }
                }
            }
            //save the path of the backup file
            PlayerPrefs.SetString("Opti_backupFolder", m_backUpPath);
            PlayerPrefs.SetInt("Opti_allowReport", ( m_AllowToGenerateReport)?1:0);
        }

        /// <summary>
        /// Handle the editor gui logic
        /// </summary>
        private void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            //propose to setup a backup folder
            #region backup
            EditorGUILayout.LabelField("Before you start, we recommend to set a backup folder to save each asset you will change");
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("...", GUILayout.Width(30)))
                m_backUpPath = EditorUtility.OpenFolderPanel("Setup a backup", (string.IsNullOrEmpty(m_backUpPath)) ? System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) :
                                                            m_backUpPath, "");
            m_backUpPath = EditorGUILayout.TextField(m_backUpPath);
            EditorGUILayout.EndHorizontal();
            m_AllowToGenerateReport = EditorGUILayout.ToggleLeft("generate a report (csv) after checking at all textures", m_AllowToGenerateReport);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            #endregion
            if (GUILayout.Button("Check all sprites"))
            {
                if (string.IsNullOrEmpty(m_assetPath))
                    m_assetPath = Application.dataPath + Path;
                CheckAll();
            }
            //var l_rect = EditorGUILayout.BeginVertical();
            //EditorGUI.ProgressBar(l_rect, s_loadingPct, s_loadingText);
            //GUILayout.Space(16);
            //EditorGUILayout.EndVertical();

            //display the data of the textures
            if (m_textures != null && m_textures.Length > 0)
            {
                #region panel buttons
                EditorGUILayout.BeginHorizontal();
                m_currentWindow = GUILayout.Toolbar(m_currentWindow, new[] { "All", "Wrong size", "Warnings", "Others" });
                EditorGUILayout.EndHorizontal();
                if (m_currentWindow < 2)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("");
                    if (!m_allReadable)
                    {
                        if (GUILayout.Button("Set all readable"))
                        {
                            for (var l_i = 0; l_i < m_textures.Length; l_i++)
                            {
                                if (m_textures[l_i].Status == SpriteStatus.NotMultiple)
                                {
                                    m_textures[l_i].Importer.isReadable = true;
                                    m_textures[l_i].Importer.SaveAndReimport();
                                }
                            }
                            m_allReadable = true;
                        }
                    }
                    else
                    {
                        if (GUILayout.Button("Reset readable options"))
                        {
                            for (var l_i = 0; l_i < m_textures.Length; l_i++)
                            {
                                if (m_textures[l_i].Status == SpriteStatus.NotMultiple)
                                {
                                    m_textures[l_i].Importer.isReadable = m_textures[l_i].IsReadable;
                                    m_textures[l_i].Importer.SaveAndReimport();
                                }
                            }
                            m_allReadable = false;
                        }
                    }

                    GUI.enabled = m_allReadable;
                    if (GUILayout.Button("Optimise all"))
                    {
                        if (EditorUtility.DisplayDialog("Warning", $"Before doing this, we recommend to have a versioning system with all of you asset up to date", "continue", "cancel"))
                        {
                            for (var l_i = 0; l_i < m_textures.Length; l_i++)
                            {
                                if (m_textures[l_i].Status == SpriteStatus.NotMultiple)
                                {
                                    Optimize(m_textures[l_i]);
                                    AssetDatabase.Refresh();
                                }
                            }
                            m_allReadable = false;
                        }
                    }
                    GUI.enabled = true;

                    EditorGUILayout.LabelField("");
                    EditorGUILayout.EndHorizontal();
                }
                #endregion

                EditorGUILayout.LabelField($"{m_fileNumber}/{m_textures.Length} assets need you attention");
                EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
                m_scroll = EditorGUILayout.BeginScrollView(m_scroll);

                //display the information of the Texture
                EditorGUILayout.BeginVertical();
                for (var l_i = 0; l_i < m_textures.Length; l_i++)
                {
                    switch (m_currentWindow)
                    {
                        case 0:
                            if (m_textures[l_i].Status != SpriteStatus.Fine)
                            {
                                m_textures[l_i].Display(this);
                            }
                            break;
                        case 1:
                            if (m_textures[l_i].Status == SpriteStatus.NotMultiple)
                            {
                                m_textures[l_i].Display(this);
                            }
                            break;
                        case 2:
                            if (m_textures[l_i].Status == SpriteStatus.HighPpu)
                            {
                                m_textures[l_i].Display(this);
                            }
                            break;
                        case 3:
                            if (m_textures[l_i].Status == SpriteStatus.NotSupported)
                            {
                                m_textures[l_i].Display(this);
                            }
                            break;
                        default:
                            m_textures[l_i].Display(this);
                            break;
                    }
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// Check how all Texture can be optimized and make a report out of it
        /// </summary>
        private void CheckAll()
        {
            //we find all Texture2D in the folder Asset
            var l_itemsId = AssetDatabase.FindAssets("t:Texture2D", m_filter);
            if (l_itemsId == null || l_itemsId.Length <= 0)
            {
                Debug.LogError("There are no Texture in the project");
                return;
            }
            m_textures = new TextureData[l_itemsId.Length];

            //array that contain all line for the report
            var l_result = new string[l_itemsId.Length + 1];
            if (m_AllowToGenerateReport)
                l_result[0] = Column;
            //FileInfo l_fi;
            m_fileNumber = 0;

            for (var l_i = 0; l_i < m_textures.Length; l_i++)
            {
                //check all reference about the Texture
                m_textures[l_i] =
                    new TextureData(AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(l_itemsId[l_i])))
                    {
                        ItemId = l_itemsId[l_i]
                    };
                m_textures[l_i].Dimension = new Vector2Int(m_textures[l_i].Texture.width, m_textures[l_i].Texture.height);
                m_textures[l_i].Path = AssetDatabase.GetAssetPath(m_textures[l_i].Texture.GetInstanceID());

                try
                {
                    m_textures[l_i].Importer = (TextureImporter)AssetImporter.GetAtPath(m_textures[l_i].Path);
                }
                catch
                {
                    Debug.LogWarning("The texture " + m_textures[l_i].Texture + " is not supported by this plugin");
                }

                var l_length = m_textures[l_i].Path.Split('/').Length;
                m_textures[l_i].Name = m_textures[l_i].Path.Split('/')[l_length - 1];
                //if a TextureImporter is found, check how to optimize it 
                if (m_textures[l_i].Importer)
                {
                    m_textures[l_i].IsReadable = m_textures[l_i].Importer.isReadable;
                    //l_fi = new FileInfo(s_names[i]);

                    //check if the dimension of the Texture are multiple of 4
                    if (m_textures[l_i].Dimension.x % 4 != 0 || m_textures[l_i].Dimension.y % 4 != 0)
                    {
                        m_textures[l_i].Status = SpriteStatus.NotMultiple;
                        m_textures[l_i].Recommended = new Vector2Int(m_textures[l_i].Dimension.x + (4 - m_textures[l_i].Dimension.x % 4), m_textures[l_i].Dimension.y + (4 - m_textures[l_i].Dimension.y % 4));
                        if (m_textures[l_i].Recommended.x - m_textures[l_i].Dimension.x > 3) m_textures[l_i].Recommended.x -= 4;
                        if (m_textures[l_i].Recommended.y - m_textures[l_i].Dimension.y > 3) m_textures[l_i].Recommended.y -= 4;
                        m_textures[l_i].Comment = "Only texture with width/height being multiple of 4 can be compressed to DXT5 format.";

                        if (m_AllowToGenerateReport)
                            l_result[l_i + 1] =
                            $"{m_textures[l_i].Path};{m_textures[l_i].Dimension};{m_textures[l_i].Recommended};{m_textures[l_i].Comment}";

                        m_fileNumber++;
                    }
                    //check if the pixel per unit is to high 
                    else if (m_textures[l_i].Importer.textureType == TextureImporterType.Sprite && m_textures[l_i].Importer.spritePixelsPerUnit > 256)
                    {
                        m_textures[l_i].Status = SpriteStatus.HighPpu;
                        m_textures[l_i].Ppu = m_textures[l_i].Importer.spritePixelsPerUnit;
                        m_textures[l_i].RecommendedPpu = m_textures[l_i].Importer.spritePixelsPerUnit;
                        var l_multiplicator = 1;
                        while (m_textures[l_i].RecommendedPpu > 256)
                        {
                            m_textures[l_i].RecommendedPpu = m_textures[l_i].RecommendedPpu / 2;
                            l_multiplicator *= 2;
                        }
                        m_textures[l_i].Recommended = new Vector2Int(m_textures[l_i].Dimension.x / l_multiplicator, m_textures[l_i].Dimension.y / l_multiplicator);

                        m_textures[l_i].Comment =
                            $"You have a high PPU(pixels per unit). See if you can lower it to {m_textures[l_i].RecommendedPpu} and change the dimension to the recommended ones. " +
                            "This will decrease the weight of the sprite, but also the quality a bit";
                        if (m_AllowToGenerateReport)
                            l_result[l_i + 1] =
                            $"{m_textures[l_i].Path};{m_textures[l_i].Dimension};{m_textures[l_i].Recommended};{m_textures[l_i].Comment}";

                        m_fileNumber++;
                    }
                    //else, the Texture is ok
                    else
                    {
                        m_textures[l_i].Status = SpriteStatus.Fine;
                        m_textures[l_i].Recommended = Vector2Int.zero;
                        m_textures[l_i].Comment = "No change required";
                        if (m_AllowToGenerateReport)
                            l_result[l_i + 1] =
                            $"{m_textures[l_i].Path};{m_textures[l_i].Dimension};/;{m_textures[l_i].Comment}";
                    }
                }
                //else, the Texture is considered to be "Not Supported"
                else
                {
                    m_textures[l_i].Status = SpriteStatus.NotSupported;
                    m_textures[l_i].Recommended = Vector2Int.zero;
                    m_textures[l_i].Comment = "For now, this asset is not supported by the plugin";
                    if (m_AllowToGenerateReport)
                        l_result[l_i + 1] =
                        $"{m_textures[l_i].Path};{m_textures[l_i].Dimension};/;{m_textures[l_i].Comment}";
                }

            }
            if(m_AllowToGenerateReport)
                File.WriteAllLines(m_assetPath, l_result, System.Text.Encoding.UTF8);
            AssetDatabase.Refresh();

        }

        /// <summary>
        /// Optimize the asset depending of it's status 
        /// </summary>
        /// <param name="p_texture">the asset to optimize</param>
        public void Optimize(TextureData p_texture)
        {
            if (p_texture.Status != SpriteStatus.NotMultiple)
                return;
            //if backup, we save the asset before doing anything
            if (!string.IsNullOrEmpty(m_backUpPath))
            {
                p_texture.Save(m_backUpPath);
            }
            switch (p_texture.Status)
            {
                case SpriteStatus.Fine:

                    break;
                case SpriteStatus.NotMultiple:

                    //calculate the offset of the texture depending of the position of the pivot
                    var l_offset = new Vector2Int((int)((p_texture.Dimension.x % 4) * (p_texture.Importer.spritePivot.x)), 
                        (int)((p_texture.Dimension.y % 4) * (p_texture.Importer.spritePivot.y)));


                    //get all pixels of the texture
                    var l_basePixels = p_texture.Texture.GetPixels();
                    //resize the texture with the recommended dimension
                    p_texture.Texture.Resize(p_texture.Recommended.x, p_texture.Recommended.y);

                    //rework the added pixels to be transparent
                    var l_alphaPixels = p_texture.Texture.GetPixels();
                    for (var l_i = 0; l_i < l_alphaPixels.Length; l_i++)
                    {
                        l_alphaPixels[l_i] = new Color(0, 0, 0, 0);
                    }
                    p_texture.Texture.SetPixels(l_alphaPixels);
                    //replace all pixels in the Texture and save it
                    p_texture.Texture.SetPixels(l_offset.x, l_offset.y, p_texture.Dimension.x, p_texture.Dimension.y, l_basePixels);
                    p_texture.Texture.Apply();
                    p_texture.Save(Application.dataPath);

                    //apply all change to the data and the TextureImporter
                    p_texture.Importer = (TextureImporter)AssetImporter.GetAtPath(p_texture.Path);
                    p_texture.Dimension = p_texture.Recommended;
                    p_texture.Comment = "No change required";
                    p_texture.Importer.isReadable = p_texture.IsReadable;
                    p_texture.Importer.SaveAndReimport();
                    p_texture.Status = SpriteStatus.Fine;
                    m_fileNumber--;
                    break;
                case SpriteStatus.HighPpu:

                    break;
                case SpriteStatus.NotSupported:

                    break;
            }
        }
    }

    /// <summary>
    /// A class to store data about a texture
    /// </summary>
    public class TextureData
    {
        #region variables
        /// <summary>
        /// The readonly state of the texture
        /// </summary>
        public readonly Texture2D Texture;
        /// <summary>
        /// The itemId of the Texture
        /// </summary>
        public string ItemId;
        /// <summary>
        /// The TextureImporter of the texture
        /// </summary>
        public TextureImporter Importer;
        /// <summary>
        /// The path in the Assets folder
        /// </summary>
        public string Path;
        /// <summary>
        /// The name of the texture
        /// </summary>
        public string Name;
        /// <summary>
        /// Determine if and how we need to optimize the Texture
        /// </summary>
        public SpriteStatus Status;
        /// <summary>
        /// The comment to display to the user
        /// </summary>
        public string Comment;
        /// <summary>
        /// Remember if the file was readable or not before the optimization
        /// </summary>
        public bool IsReadable;
        /// <summary>
        /// The Pixel Per Unit of the sprite
        /// </summary>
        public float Ppu;
        /// <summary>
        /// The recommended Pixel Per Unit for the sprite
        /// </summary>
        public float RecommendedPpu;
        /// <summary>
        /// The dimension of the texture
        /// </summary>
        public Vector2Int Dimension;
        /// <summary>
        /// The recommended dimension for the Texture
        /// </summary>
        public Vector2Int Recommended;
        /// <summary>
        /// Whether or not the texture details in the editor is fold out
        /// </summary>
        private bool m_foldout;
        #endregion

        #region Constructors

        public TextureData(Texture2D p_texture)
        {
            Texture = p_texture;
        }
        #endregion

        /// <summary>
        /// Display the information of the texture in the editor 
        /// </summary>
        /// <param name="p_launcher">The window where the information are displayed</param>
        public void Display(SpriteChecker p_launcher)
        {
            //change the color of the foldout depending of the status
            m_foldout = EditorGUILayout.Foldout(m_foldout, Name, Status == SpriteStatus.NotSupported ? p_launcher.m_notSupStyle : p_launcher.m_statusStyle[(int)Status]);

            if (m_foldout)
            {
                //display the Texture
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(Texture), GUILayout.Width(64),GUILayout.Height(64));
                EditorGUILayout.BeginVertical();
                //display some data depending of the status
                switch (Status)
                {
                    //for the not multiple of 4, display only the sizes
                    case SpriteStatus.NotMultiple:
                        EditorGUILayout.LabelField("Dimension", Dimension.ToString());
                        EditorGUILayout.LabelField("Recommended", Recommended.ToString());
                        break;
                    //for the to high PPU, we display the sizes and the PPU
                    case SpriteStatus.HighPpu:
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Dimension", Dimension.ToString());
                        EditorGUILayout.LabelField("PPU", Ppu.ToString(CultureInfo.InvariantCulture));
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Recommended", Recommended.ToString());
                        EditorGUILayout.LabelField("Recommended PPU", RecommendedPpu.ToString(CultureInfo.InvariantCulture));
                        EditorGUILayout.EndHorizontal();
                        break;
                    case SpriteStatus.NotSupported:
                        break;
                    case SpriteStatus.Fine:
                        break;
                }

                EditorGUILayout.LabelField(Comment);
                EditorGUILayout.BeginHorizontal();
                //button to select the asset in the project
                if (GUILayout.Button("Select"))
                    Selection.activeObject = AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(ItemId));
                //button to optimise the asset
                if (Status == SpriteStatus.NotMultiple)
                {
                    if (Importer.isReadable)
                    {
                        if (GUILayout.Button("Optimise"))
                        {
                            p_launcher.Optimize(this);
                            AssetDatabase.Refresh();
                        }
                    }
                    else
                    {
                        if (GUILayout.Button("set readable to optimise"))
                        {
                            Importer.isReadable = true;
                            Importer.SaveAndReimport();
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        }

        /// <summary>
        /// Save the file before or after modifications
        /// </summary>
        /// <param name="p_basePath">The base path where to save the texture</param>
        public void Save(string p_basePath)
        {
            //we will encode the Texture into an array of bytes depending of the extension of the file
            byte[] l_bytes;

            var l_length = Path.Split('.').Length;
            if (string.Equals(Path.Split('.')[l_length - 1], "png", System.StringComparison.Ordinal))
                l_bytes = Texture.EncodeToPNG();
            else if (string.Equals(Path.Split('.')[l_length - 1], "jpg", System.StringComparison.Ordinal))
                l_bytes = Texture.EncodeToJPG();
            else
            {
                Debug.LogError("We can't save it");
                return;
            }
            //after, we generate the path to save the asset
            l_length = Path.Split('/').Length;
            var l_path = p_basePath;
            var l_directory = p_basePath;
            for (var l_i = 1; l_i < l_length; l_i++)
            {
                if (l_i == l_length - 1)
                    l_directory = l_path;
                l_path += "/" + Path.Split('/')[l_i];
            }
            //quick check if the Directory exist
            if (!Directory.Exists(l_directory))
                Directory.CreateDirectory(l_directory);
            //save the asset
            File.WriteAllBytes(l_path, l_bytes);
        }
    }

    /// <summary>
    /// The current status of the sprite
    /// </summary>
    public enum SpriteStatus
    {
        /// <summary>
        /// This sprite is not supported by the editor
        /// </summary>
        NotSupported = -1,
        /// <summary>
        /// This sprite is already optimized
        /// </summary>
        Fine = 0,
        /// <summary>
        /// This sprite width and/or length are not multiple of four
        /// </summary>
        NotMultiple = 1,
        /// <summary>
        /// This sprite has an high pixel per unit
        /// </summary>
        HighPpu = 2,
    }
}